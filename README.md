ansible-role-aerc
=========

This role simply compiles and installs the [aerc mail client](https://git.sr.ht/~sircmpwn/aerc)

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
